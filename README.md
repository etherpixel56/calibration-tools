### Calibration Tools
---

#### About

Calibration Tools is a Windows application which manages your monitor calibration.  It was created to provide more control over monitor calibration.

#### Features

* Create and edit monitor calibration files: adjust backlight, white balance, overall gamma, shadow detail gamma, 10-band gamma, s-curve, highlights.
* Enforce separate day/night files with automatic dawn/dusk times and gradual transition during twilight.
* Test for calibration hijacking in games, and solve with ReShade or borderless full screen windowed mode.
* Enforce per-game calibration files.
* Enable dithering on Nvidia GPUs for smoother gradients.
* Portable, malware-free, ad-free, nag-free.

#### System Requirements

* 64-bit Windows, Vista or later.
* Logged in as a Windows administrator.
* Video card supporting gamma tables for monitor calibration.
* DirectX June 2010 update (www.microsoft.com/en-us/download/details.aspx?id=8109).
* **Note: Windows 10 version 1903 (May 2019) contains bugs in its colour management system which affect Calibration Tools.  For possible solutions, see [here](https://hub.displaycal.net/forums/topic/windows-10-1903-please-read/).**
  
#### Screenshots

![Alt text](https://bitbucket.org/CalibrationTools/images/downloads/screenshots.png)

#### Donate
Bitcoin: 1CALTLSdHctke7hryfMuG43EqVv7gSP88z

![Alt text](https://bitbucket.org/CalibrationTools/images/downloads/qr178bl.png)

Thank you!